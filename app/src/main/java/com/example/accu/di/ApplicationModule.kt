package com.example.accu.di

import android.content.Context
import com.example.accu.data.remote.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
class ApplicationModule {
    @Provides
    fun ProvideOkhttpClient(@ApplicationContext context: Context):OkHttpClient{
        val httpLoggingInterceptor=HttpLoggingInterceptor()
        httpLoggingInterceptor.level=HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).build()

    }
    @Provides
    fun ProvideRetrofit(okHttpClient: OkHttpClient):Retrofit=Retrofit
        .Builder().baseUrl("http://infotechdemos.com/darfi/api/")
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttpClient)
        .build()

    @Provides
    fun ProvideService(retrofit: Retrofit):ApiService=retrofit.create(ApiService::class.java)

}