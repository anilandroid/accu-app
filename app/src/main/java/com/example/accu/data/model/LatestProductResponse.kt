package com.example.accu.data.model

class LatestProductResponse(
    val message: String,
    val success: Boolean
) {
}