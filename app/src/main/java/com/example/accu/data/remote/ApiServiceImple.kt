package com.example.accu.data.remote

import com.example.accu.data.model.LatestProductResponse
import javax.inject.Inject

class ApiServiceImple @Inject constructor(private val apiService: ApiService) {
    suspend fun getdata():LatestProductResponse= apiService.getLatestProduct()
}
