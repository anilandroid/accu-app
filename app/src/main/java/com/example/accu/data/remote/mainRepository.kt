package com.example.accu.data.remote

import com.example.accu.data.model.LatestProductResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import java.util.concurrent.Flow
import javax.inject.Inject

class mainRepository @Inject constructor(private val apiServiceImple: ApiServiceImple) {
     fun getData():kotlinx.coroutines.flow.Flow<LatestProductResponse> = flow{
         emit(apiServiceImple.getdata())
     }.flowOn(Dispatchers.IO)
}