package com.example.accu.data.remote

import com.example.accu.data.model.LatestProductResponse
import retrofit2.http.GET

interface ApiService{

    @GET("advertisement/latest")
    suspend fun getLatestProduct(): LatestProductResponse

}