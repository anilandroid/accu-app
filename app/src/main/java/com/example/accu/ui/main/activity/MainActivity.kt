package com.example.accu.ui.main.activity

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.example.accu.R
import com.example.accu.Utils.HelperMethods
import com.example.accu.databinding.ActivityMainBinding
import com.example.accu.ui.main.fragment.*
import com.google.android.material.navigation.NavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private var drawerLayout: DrawerLayout? = null
    private var toggle: ActionBarDrawerToggle? = null
    private var navigationView: NavigationView? = null
    var navigatebutton: ImageView? = null
    var btncheckin:ImageView?=null
    private val binding: ActivityMainBinding? = null
    var fragment:Fragment?=null




    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }



    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun init() {
        drawerLayout = findViewById<View>(R.id.main) as DrawerLayout
        toggle = ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close)
        drawerLayout!!.addDrawerListener(toggle!!)
        toggle!!.syncState()
        navigatebutton = findViewById<View>(R.id.menubutton) as ImageView
        navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        btncheckin=findViewById(R.id.btncheckin)
        btncheckin!!.setOnClickListener(View.OnClickListener {
            val bundle=Bundle()
            bundle.putString("type","1")
            fragment=ScanCode()
            fragment!!.arguments=bundle
            HelperMethods.switchwithaddfragment(fragment, supportFragmentManager)
        })
        navigationView!!.setNavigationItemSelectedListener(NavigationView.OnNavigationItemSelectedListener { item ->
            val fragment: Fragment
            val fragmentManager = supportFragmentManager
            val id = item.itemId
            when (id) {
                R.id.about -> {
                    fragment = About()
                    HelperMethods.switchwithaddfragment(fragment, fragmentManager)
                    drawerLayout!!.closeDrawers()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.contact -> {
                    HelperMethods.switchwithaddfragment(Contact(),fragmentManager)
                    drawerLayout!!.closeDrawers()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.checkin -> {
                    HelperMethods.switchfragment(CheckIn(),fragmentManager)
                    drawerLayout!!.closeDrawers()
                    return@OnNavigationItemSelectedListener true
                }
                R.id.checkout -> {
                    HelperMethods.switchfragment(Checkout(),fragmentManager)
                    drawerLayout!!.closeDrawers()
                    return@OnNavigationItemSelectedListener true
                }

                R.id.help -> {
                    HelperMethods.switchwithaddfragment(Help(),fragmentManager)
                    drawerLayout!!.closeDrawers()
                    return@OnNavigationItemSelectedListener true
                }
                else -> return@OnNavigationItemSelectedListener true
            }
            true
        })
        navigatebutton!!.setOnClickListener {
            if (drawerLayout!!.isDrawerOpen(Gravity.LEFT)) {
                drawerLayout!!.closeDrawer(Gravity.LEFT)
            } else {
                drawerLayout!!.openDrawer(Gravity.LEFT)
            }
        }
    }

    override fun onBackPressed() {
        if(supportFragmentManager.backStackEntryCount>0){
            supportFragmentManager.popBackStack()
        }else{
            shutdown()
        }
    }


    fun shutdown() {
        var dialog: AlertDialog.Builder = AlertDialog.Builder(this)
        dialog.setMessage(getString(R.string.exit_message))
        dialog.setTitle(getString(R.string.exit))
        dialog.setPositiveButton(getString(R.string.yes), DialogInterface.OnClickListener { dialog, which ->
            dialog.cancel()
            finish()
        })
        dialog.setNegativeButton(getString(R.string.No), DialogInterface.OnClickListener { dialog, which ->
            dialog.cancel()
        })
        dialog.show()
    }

}