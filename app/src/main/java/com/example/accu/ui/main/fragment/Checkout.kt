package com.example.accu.ui.main.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.accu.R
import com.example.accu.Utils.HelperMethods
import com.example.accu.ui.main.activity.MainActivity

class Checkout : Fragment() {
    var rootview:View?=null
    var btncheckin: ImageView?=null
    var fragment:Fragment?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootview= inflater.inflate(R.layout.fragment_checkout, container, false)
        init()
        return rootview
    }

    fun init(){
        btncheckin=rootview!!.findViewById(R.id.btncheckin)
        btncheckin!!.setOnClickListener(View.OnClickListener {
            val bundle=Bundle()
            bundle.putString("type","3")
            fragment=ScanCode()
            fragment!!.arguments=bundle
            HelperMethods.switchwithaddfragment(fragment, requireActivity()!!.supportFragmentManager)
        })
    }
}