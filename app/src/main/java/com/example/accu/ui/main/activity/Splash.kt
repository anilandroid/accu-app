package com.example.accu.ui.main.activity

import android.Manifest.permission
import android.Manifest.permission.*
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.accu.R
import com.example.accu.Utils.soringProgram
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

import androidx.core.app.ActivityCompat

import android.content.pm.PackageManager
import android.graphics.Camera

import androidx.core.content.ContextCompat
import android.content.DialogInterface

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.app.AlertDialog

import androidx.core.app.ActivityCompat.requestPermissions

import android.os.Build
import androidx.constraintlayout.widget.ConstraintLayout

import com.google.android.material.snackbar.Snackbar


class Splash : AppCompatActivity() {
    var mainconstraint:ConstraintLayout?=null
    var permsRequestCode = 200

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        soringProgram.sotProgram()
        if(!checkPermission()){
            requestPermission()
        }else{
            GlobalScope.launch(context = Dispatchers.IO) {
                Thread.sleep(3000)
                openActivity()
            }
        }
        init()
    }

    fun init(){
        mainconstraint=findViewById(R.id.mainconstraint)
    }


    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(applicationContext, CAMERA)
        val result1 = ContextCompat.checkSelfPermission(applicationContext, READ_EXTERNAL_STORAGE)
        val result2 = ContextCompat.checkSelfPermission(applicationContext, WRITE_EXTERNAL_STORAGE)

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(CAMERA,READ_EXTERNAL_STORAGE,WRITE_EXTERNAL_STORAGE),
            permsRequestCode
        )
    }



    override fun onResume() {
        super.onResume()

    }

    private fun openActivity() {
          startActivity(Intent(this, MainActivity::class.java))
          finish()
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            permsRequestCode -> if (grantResults.size > 0) {
                val locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED
                val cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED
                val permission2= grantResults[2] == PackageManager.PERMISSION_GRANTED
                if (locationAccepted && cameraAccepted && permission2) {
                    Snackbar.make(mainconstraint!!, "Permission Granted, Now you can access local  data and camera.", Snackbar.LENGTH_LONG).show()
                    openactivity()
                }
                else {
                    Snackbar.make(mainconstraint!!, "Permission Denied, You cannot access location data and camera.", Snackbar.LENGTH_LONG).show()
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION)) {
                            showMessageOKCancel(
                                "You need to allow access to both the permissions") { dialog, which ->
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(
                                        arrayOf(ACCESS_FINE_LOCATION, CAMERA),
                                        permsRequestCode
                                    )
                                }
                            }
                            return
                        }
                    }
                }
            }
        }
    }




    private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this@Splash)
            .setMessage(message)
            .setPositiveButton("OK", okListener)
            .setNegativeButton("Cancel", null)
            .create()
            .show()
    }

    fun openactivity(){
        GlobalScope.launch(context = Dispatchers.IO) {
            Thread.sleep(3000)
            openActivity()
        }
    }

}