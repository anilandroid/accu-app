package com.example.accu.ui.main.fragment

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.RetryPolicy
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.CodeScannerView
import com.budiyev.android.codescanner.DecodeCallback
import com.example.accu.R
import com.example.accu.Utils.HelperMethods
import java.text.SimpleDateFormat
import java.util.*


class ScanCode : Fragment() {

    var showQRCode: LinearLayout? = null
    var rootview: View? = null
    private var mCodeScanner: CodeScanner? = null
    var user_id: String? = null
    var type:String?=null
    var fragment:Fragment?=null
    var scannerView: CodeScannerView?=null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.fragment_scan_code, container, false)
        init()
        return rootview
    }

    override fun onResume() {
        super.onResume()
        mCodeScanner!!.startPreview()
    }


    override fun onPause() {
        mCodeScanner!!.releaseResources()
        super.onPause()
    }

    fun init() {
        val bundle=arguments
        type=bundle!!.getString("type")
        val currentDateAndTime: String = SimpleDateFormat("HH:mm").format(Date())
        val currentDateAndDate: String = SimpleDateFormat("dd-MM-yyyy").format(Date())

        scannerView = rootview!!.findViewById(R.id.scanner_view)
        mCodeScanner = CodeScanner(requireActivity(), scannerView!!)
        //showQRCode=rootview!!.findViewById(R.id.showQRCode)
        //HelperMethods.switchwithaddfragment(ScanDetails(),requireActivity().supportFragmentManager)
        mCodeScanner!!.startPreview()
        mCodeScanner!!.decodeCallback = DecodeCallback { result ->
            requireActivity().runOnUiThread {
                user_id = result.text.toString()
                try {
                    if (type.equals("1") || type.equals("2"))
                    {
                        loginUser(user_id.toString(), currentDateAndTime, currentDateAndDate)
                    }else if (type.equals("3")){

                        logoutUser(user_id.toString(), currentDateAndTime, currentDateAndDate)
                    }
                } catch (e: Exception) {
                    Toast.makeText(
                        activity,
                        result.text + "This is not a valid container",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
        scannerView!!.setOnClickListener(View.OnClickListener {
            mCodeScanner!!.startPreview()
        })
    }

    private fun logoutUser(userId: String, currentDateAndTime: String, currentDateAndDate: String) {
        val loading = ProgressDialog.show(requireContext(), "Loading", "Please wait...")

        val stringRequest: StringRequest = object : StringRequest(
            Method.POST,
            "https://script.google.com/macros/s/AKfycbyNyzONlDWzVx-wI0DJ0JlV0jc71b5q8o6SUXo7-pmn0f1OUuQJd-3oirMd8UWVgD3N/exec",
            Response.Listener { response ->
                if(!response.equals("Your have done OUT today"))
                {
                    loading.dismiss()
                    Toast.makeText(requireContext(), "" +response, Toast.LENGTH_LONG).show()
                    val bundle=Bundle()
                    bundle.putString("type","2")
                    bundle.putString("id",userId)
                    bundle.putString("date",currentDateAndDate)
                    bundle.putString("time",currentDateAndTime)
                    fragment=ScanDetails()
                    fragment!!.arguments=bundle
                    HelperMethods.switchwithaddfragment(fragment, requireActivity()!!.supportFragmentManager)
                }else{
                    loading.dismiss()
                    mCodeScanner!!.startPreview()
                    Toast.makeText(requireContext(), "" + response, Toast.LENGTH_LONG).show()
                }

                /* Toast.makeText(requireContext(), response, Toast.LENGTH_LONG).show()
                 val intent = Intent(getApplicationContext(), MainActivity::class.java)
                 startActivity(intent)*/
            },
            Response.ErrorListener {
            }
        ) {
            override fun getParams(): Map<String, String> {
                val parmas: MutableMap<String, String> = HashMap()

                //here we pass params
                parmas["action"] = "doPost"
                parmas["ID"] = userId
                parmas["OUT"] = currentDateAndTime
                return parmas
            }
        }

        val socketTimeOut = 50000 // u can change this .. here it is 50 seconds


        val retryPolicy: RetryPolicy =
            DefaultRetryPolicy(socketTimeOut, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = retryPolicy
        val queue = Volley.newRequestQueue(requireContext())
        queue.add(stringRequest)
    }

    private fun loginUser(userId: String, currentDateAndTime: String, currentDateAndDate: String) {
          val loading = ProgressDialog.show(requireContext(), "Loading", "Please wait...")

        val stringRequest: StringRequest = object : StringRequest(
            Method.POST,
            "https://script.google.com/macros/s/AKfycbyxNQxnYucM2eL8uawLUoVDhSJIiSYYE7EiY1nEJbe8wixlYmz5622wlDfwT3yS9gf9/exec",
            Response.Listener { response ->
                if (!response.equals("You are already IN")) {
                    loading.dismiss()
                    Toast.makeText(requireContext(), "" + response, Toast.LENGTH_LONG).show()
                    val bundle = Bundle()
                    bundle.putString("type", "1")
                    bundle.putString("id", userId)
                    bundle.putString("date", currentDateAndDate)
                    bundle.putString("time", currentDateAndTime)
                    fragment = ScanDetails()
                    fragment!!.arguments = bundle
                    HelperMethods.switchwithaddfragment(
                        fragment,
                        requireActivity()!!.supportFragmentManager
                    )
                }else{
                    loading.dismiss()
                    mCodeScanner!!.startPreview()
                    Toast.makeText(requireContext(), "" + response, Toast.LENGTH_LONG).show()
                }

                //requireActivity().supportFragmentManager.popBackStack()
                /* Toast.makeText(requireContext(), response, Toast.LENGTH_LONG).show()
                 val intent = Intent(getApplicationContext(), MainActivity::class.java)
                 startActivity(intent)*/
            },
            Response.ErrorListener {
            }
        ) {
            override fun getParams(): Map<String, String> {
                val parmas: MutableMap<String, String> = HashMap()

                //here we pass params
                parmas["action"] = "addItem"
                parmas["ID"] = userId
                parmas["IN"] = currentDateAndTime
                return parmas
            }
        }

        val socketTimeOut = 50000 // u can change this .. here it is 50 seconds


        val retryPolicy: RetryPolicy =
            DefaultRetryPolicy(socketTimeOut, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest.retryPolicy = retryPolicy

        val queue = Volley.newRequestQueue(requireContext())
        queue.add(stringRequest)
    }
}