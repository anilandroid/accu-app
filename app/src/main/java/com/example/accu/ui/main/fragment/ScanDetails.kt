package com.example.accu.ui.main.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.accu.R
import com.example.accu.Utils.ApiState
import com.example.accu.data.remote.mainRepository
import com.example.accu.ui.main.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class ScanDetails : Fragment() {
    //private val viewModel by viewModels<MainViewModel>()
    var root:View?=null
    var marked_text:TextView?=null
    var marked_date:TextView?=null
    var marked_time:TextView?=null
    var date:TextView?=null
    var time:TextView?=null
    var id:TextView?=null
    var image:ImageView?=null

    // Get Data
    var type:String?=null
    var user_id:String?=null
    var user_date:String?=null
    var user_time:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        root= inflater.inflate(R.layout.fragment_scan_details, container, false)
        return root
        /*viewModel.getPost()
        lifecycleScope.launchWhenCreated {
            viewModel._UiStateFlow.collect {
                when(it){
                    is ApiState.Loading->{

                    }
                    is ApiState.Failure->{

                    }
                    is ApiState.Success->{

                    }
                    is ApiState.Empty->{

                    }
                }
            }
        }*/
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        marked_text=root!!.findViewById(R.id.mark)
        marked_date=root!!.findViewById(R.id.marked_date)
        marked_time=root!!.findViewById(R.id.marked_time)
        date=root!!.findViewById(R.id.date)
        time=root!!.findViewById(R.id.time)
        id=root!!.findViewById(R.id.user_id)
        image=root!!.findViewById(R.id.checked_icon)

        val bundle=arguments

        type=bundle!!.getString("type")
        user_id=bundle!!.getString("id")
        user_date=bundle!!.getString("date")
        user_time=bundle!!.getString("time")

        if (type.equals("1"))
        {
            marked_text!!.text=getString(R.string.attendanceMarked)
            marked_date!!.text="Marked Date"
            marked_time!!.text="Marked Time"
            id!!.text=user_id.toString()
            date!!.text=user_date.toString()
            time!!.text=user_time.toString()
            image!!.setImageResource(R.drawable.clocked_in)

        }else if (type.equals("2"))
        {
            marked_text!!.text=getString(R.string.attendanceUnmarked)
            marked_date!!.text="Logout Date"
            marked_time!!.text="Logout Time"
            id!!.text=user_id.toString()
            date!!.text=user_date.toString()
            time!!.text=user_time.toString()
            image!!.setImageResource(R.drawable.clockedout)
        }
    }
}