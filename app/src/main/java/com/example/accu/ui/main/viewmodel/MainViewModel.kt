package com.example.accu.ui.main.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.accu.Utils.ApiState
import com.example.accu.data.remote.mainRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val mainRepository: mainRepository):ViewModel() {
    private val DataStateFlow:MutableStateFlow<ApiState> = MutableStateFlow(ApiState.Empty)
    val _UiStateFlow:StateFlow<ApiState> = DataStateFlow

    fun getPost() = viewModelScope.launch {
        DataStateFlow.value=ApiState.Loading
        mainRepository.getData()
            .catch {
                e->
                DataStateFlow.value=ApiState.Failure(e)
            }.collect {
                data->
                DataStateFlow.value=ApiState.Success(data)
            }
    }
}