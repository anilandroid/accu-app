package com.example.accu.Utils

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.accu.R

class HelperMethods {
    companion object{
        fun switchfragment(fragment: Fragment?, fm: FragmentManager) {
            if (fragment != null) {
                fm.beginTransaction()
                    .replace(R.id.nav_host_fragment, fragment)
                    .commit()
            }
        }


        fun switchwithaddfragment(fragment: Fragment?, fm: FragmentManager) {
            if (fragment != null) {
                fm.beginTransaction()
                    .add(R.id.nav_host_fragment, fragment)
                    .addToBackStack(null)
                    .commit()
            }
        }
    }
}