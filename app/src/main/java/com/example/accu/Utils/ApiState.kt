package com.example.accu.Utils

import com.example.accu.data.model.LatestProductResponse

sealed class ApiState{
    object  Loading:ApiState()
    class Failure(val message:Throwable):ApiState()
    class  Success(val data: LatestProductResponse):ApiState()
    object Empty:ApiState()
}
